
IFstream restartFile
(
    runTime.path()/runTime.timeName()/"restart.raw"
);

if (restartFile)
{
    Info << "restart from file!!! " << endl;

    if (restartFile.good())
    {
        scalar s;
        restartFile >> s;
        runTime.setDeltaT(s);

        vector v;
        restartFile >> v;
        sError.value() = v;

        restartFile >> v;
        sCentre.value() = v;

        restartFile >> v;
        sInletVel.value() = v;

        restartFile >> v;
        sBubbleVel.value() = v;

        restartFile >> v;
        sBubbleRelVel.value() = v;

        restartFile >> v;
        sUAdjust.value() = v;

        restartFile >> v;
        sIntegralComponent.value() = v;

        restartFile >> v;
        sBubbleAcc.value() = v;

        restartFile >> v;
        sBubbleRelAcc.value() = v;

        restartFile >> v;
        sFrameAcc.value() = v;

        restartFile >> v;
        sFrameVel.value() = v;

        restartFile >> v;
        sFramePos.value() = v;

        restartFile >> v;
        sCentreTarget.value() = v;
    }
}

