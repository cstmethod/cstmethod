/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    interCSTPeriodicFoam

Authors
    Daniel Deising     < deising@mma.tu-darmstadt.de>
    All rights reserved.

Description
    Solver for 2 incompressible, isothermal immiscible fluids using a VOF
    (volume of fluid) phase-fraction based interface capturing approach,
    with optional additional species transfer for dilute species including 
    chemical reaction(s) and with optional mesh motion and mesh topology 
    changes including adaptive re-meshing and dynamic load balancing.
    Solves equation system in the center-of-moment reference frame
    (periodic pressure) for fully periodic domains (e.g. bubble swarm modeling)

    You may refer to this software as :
    //- Deising et. al. 2016, Chem. Eng. Sci. 139, pp. 173-195

    This code has been developed by :
        Daniel Deising (main developer).
    
    Method Development and Intellectual Property :
        Daniel Deising   <deising@mma.tu-darmstadt.de>
        Holger Marschall <marschall@mma.tu-darmstadt.de>
        Dieter Bothe     <bothe@mma.tu-darmstadt.de>

        Mathematical Modeling and Analysis
        Center of Smart Interfaces
        Technische Universitaet Darmstadt
       
    If you use this software for your scientific work or your publications,
    please don't forget to acknowledge explicitly the use of it.
\*---------------------------------------------------------------------------*/

#include "fvCFD.H"
#include "dynamicFvMesh.H"
#include "myMULES.H"
#include "subCycle.H"
#include "TwoPhaseMixture.H"
#include "turbulenceModel.H"
#include "interpolationTable.H"
#include "pimpleControl.H"
#include "CorrectPhi.H"

// for species transport
#include "IOobjectList.H"
#include "speciesTransfer.H"
#include "reactiveMixture.H"
#include "chemistryModel.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
    #include "setRootCase.H"
    #include "createTime.H"
    #include "createDynamicFvMesh.H"
    #include "initContinuityErrs.H"
    #include "createFields.H"
    #include "createSpecies.H"
    #include "createPressure.H"
    #include "createTimeControls.H"

    pimpleControl pimple(mesh);

    volScalarField rAU
    (
        IOobject
        (
            "rAU",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh,
        dimensionedScalar("rAUf", dimTime/rho.dimensions(), 1.0)
    );

    #include "correctPhiOld.H"
    #include "createUf.H"
    #include "CourantNo.H"
    #include "setInitialDeltaT.H"

    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
    Info<< "\nStarting time loop\n" << endl;

    while (runTime.run())
    {
        #include "readTimeControls.H"
        #include "readControls.H"
        #include "CourantNo.H"
        #include "alphaCourantNo.H"
        #include "setDeltaT.H"

        runTime++;

        Info<< "Time = " << runTime.timeName() << nl << endl;
 
        #include "refinement.H"

        if (mesh.changing() && checkMeshCourantNo)
        {
            #include "meshCourantNo.H"
        }

        #include "alphaEqnSubCycle.H"
        twoPhaseProperties.correct();

        rhom = rho.weightedAverage(mesh.Vsc());
        Info << "density rhom = " << rhom.value() << endl;

        //- re-set pRefCell
        //#include "resetPrefCell.H"

        // --- Pressure-velocity PIMPLE corrector loop
        while (pimple.loop())
        {
            Info<<"max flux divergence: " << max(mag(fvc::div(phi))) << endl;
            #include "UEqn.H"

            // --- Pressure corrector loop
            while (pimple.correct())
            {
                #include "pEqn.H"
            }

            if (pimple.turbCorr())
            {
            //-DD: turbulence models currently not available due to TwoPhaseProperties-class issues
            //    turbulence->correct();
            }
        }
        Info<<"max flux divergence: " << max(mag(fvc::div(phi))) << endl;

        runTime.write();

        Info<< "ExecutionTime = " << runTime.elapsedCpuTime() << " s"
            << "  ClockTime = " << runTime.elapsedClockTime() << " s"
            << nl << endl;
    }

    Info<< "End\n" << endl;

    return 0;
}


// ************************************************************************* //
