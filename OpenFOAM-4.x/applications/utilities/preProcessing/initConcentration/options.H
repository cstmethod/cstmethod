
    // add arguments to function call:
    timeSelector::addOptions(true, false);
    //argList::noParallel(); // this line disables use of parallel computation
#   include "addRegionOption.H"
    
    // Important: addOptions needs to be called BEFORE #include "setRootCase.H"!!!!
    // otherwise the options are not recognized!!!
    argList::addOption
    (
        "c1",
        "c1",
        "concentration in phase 1"
    );
    argList::addOption
    (
        "c2",
        "c2",
        "concentration in phase 2"
    );

    argList::addOption
    (
        "field",
        "field",
        "concentration field name"
    );

    #include "setRootCase.H"
    #include "createTime.H"

    // default field name: C0
    word fieldName("C0");
    if(args.optionFound("field"))
    {
        fieldName = args.optionRead<word>("field");
    }

    // default c1,c2: 1,0
    scalar c1(1.);
    scalar c2(0.);
    if(args.optionFound("c1"))
    {
        c1 = args.optionRead<scalar>("c1");
    }

    if(args.optionFound("c2"))
    {
        c2 = args.optionRead<scalar>("c2");
    }
