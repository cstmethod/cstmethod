
A short introduction to the code ...

steps of refinement and load balancing:
------------------------------------------

called function. update()
constructor : same as dynamicMeshDict, only sets up some empty private data members
and creates - if selected - the refinement field set-up in constant/dynamicMeshDict

bool Foam::dynamicRefineBalancedFvMesh::update()
    1) call readRefinementDict():
        creates dict dynamicMeshDict to be read from
        if refinementControls keyword found in dict, then
            read fields and store in hash table
            read gradfields and store in hash table
            read interface and store in hash table
            read curlfields and store in hash table
            read regions and store in ptr list
        else: do nothing
    2) call updateRefinementField():
        initialize field with zero
        get refinement levels from mesh cutter (hexref8: meshCutter().cellLevel())
        get lowerRefineLevel and unrefineLevel from dynamicMeshDict (subdict dynamicRefineFvMeshCoeffs)
        create marker field based on refinementControls (1)
            mark for refinement if current cellLevel < targetLevel
            at end, loop over markerField and expand iso-celLevel-layers 
            to prevent 2-to-1 refinement (based on nBufferLayers; which
            is not working in the standard version of OF)
          
