/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Version:  2.4.x                               
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    alphaInterpolation

SourceFiles
    alphaInterpolation.C

Authors
    Daniel Rettenmaier <rettenmaier@gsc.tu-darmstadt.de>
    Daniel Deising     <deising@mma.tu-darmstadt.de>
    All rights reserved.

Description
    
    You may refer to this software as :
    //- full bibliographic data to be provided

    This code has been developed by :
        Daniel Rettenmaier < rettenmaier@gsc.tu-darmstadt.de> (main developer).
    
    Method Development and Intellectual Property :
        Daniel Rettenmaier <rettenmaier@gsc.tu-darmstadt.de>
        Daniel Deising     <deising@mma.tu-darmstadt.de>
        Holger Marschall   <marschall@mma.tu-darmstadt.de>
        Dieter Bothe       <bothe@mma.tu-darmstadt.de>
        Cameron Tropea     <ctropea@sla.tu-darmstadt.de>

        Mathematical Modeling and Analysis
        Institute for Fluid Mechanics and Aerodynamics
        Center of Smart Interfaces
        Technische Universitaet Darmstadt
       
    If you use this software for your scientific work or your publications,
    please don't forget to acknowledge explicitly the use of it.

\*---------------------------------------------------------------------------*/


#include "error.H"

#include "alphaInterpolation.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

Foam::autoPtr<Foam::alphaInterpolation>Foam::alphaInterpolation::New
(
    const word& name,
    const volScalarField& alpha,
    const List<bool>& isWallPatch
)
{
    const word alphaInterpolationTypeName = name;

    Info<<"Selecting alphaInterpolation model " << alphaInterpolationTypeName << endl;

    dictionaryConstructorTable::iterator interpolIter =
        dictionaryConstructorTablePtr_->find(alphaInterpolationTypeName);

    if (interpolIter == dictionaryConstructorTablePtr_->end())
    {
        FatalErrorIn
        (
            "alphaInterpolation::New"
            "("
            "const word&"
            "const volScalarField&"
            "const List<bool>&"
            ")"
        ) << "Unknown alphaInterpolation type"
          << "alphaInterpolation" << nl << nl
          << "Valid alphaInterpolation models are : " << endl
          << dictionaryConstructorTablePtr_->sortedToc()
          << exit(FatalError);
    }

    return autoPtr<alphaInterpolation>
    (
            interpolIter()(name, alpha, isWallPatch)
    );
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// ************************************************************************* //
