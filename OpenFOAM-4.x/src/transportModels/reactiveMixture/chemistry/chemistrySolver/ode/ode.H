/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | foam-extend: Open Source CFD
   \\    /   O peration     |
    \\  /    A nd           | For copyright notice see file Copyright
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of foam-extend.

    foam-extend is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    foam-extend is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with foam-extend.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::ode

Description
    An ODE solver for chemistry

SourceFiles
    ode.C

\*---------------------------------------------------------------------------*/

#ifndef ode_H
#define ode_H

#include "chemistrySolver.H"
#include "ODESolver.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                            Class ode Declaration
\*---------------------------------------------------------------------------*/

class ode
:
    public chemistrySolver
{
    // Private data

        dictionary coeffsDict_;
        const word solverName_;
        autoPtr<ODESolver> ODESolver_;

        // Model constants

            scalar eps_;


public:

    //- Runtime type information
    TypeName("ode");


    // Constructors

        //- Construct from components
        ode
        (
            chemistryModel& model,
            const word& modelName
        );


    //- Destructor
    virtual ~ode();


    // Member Functions

        virtual scalar solve
        (
            scalarField& c,
            const scalar t0,
            const scalar dt
        ) const;
        
//copy from ode.H:
        ////- Return number of equations
        //virtual label nEqns() const;

        ////- Evaluate derivatives
        //virtual void derivatives
        //(
            //const scalar x,
            //const scalarField& y,
            //scalarField& dydx
        //) const
        //{Info<<"called derivatives" << endl;};

        ////- Evaluate Jacobian
        //virtual void jacobian
        //(
            //const scalar x,
            //const scalarField& y,
            //scalarField& dfdx,
            //scalarSquareMatrix& dfdy
        //) const
        //{Info<<"called jacobian" << endl;};
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
