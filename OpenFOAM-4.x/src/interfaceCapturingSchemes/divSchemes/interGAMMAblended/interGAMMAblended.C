/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

\*---------------------------------------------------------------------------*/

#include "interGAMMAblended.H"
#include "fvc.H"
#include "volFields.H"
#include "surfaceFields.H"
#include "upwind.H"

// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //

// Blending between Gamma and interGamma schemes
// use interGamma in narrow band around interface
Foam::scalar Foam::interGAMMAblended::weight
(
    const volScalarField& phi,
    const scalar cdWeight,
    const scalar faceFlux,
    const scalar& phiP,
    const scalar& phiN,
    const vector& gradcP,
    const vector& gradcN,
    const scalar Cof,
    const vector d,
    const point cP,
    const point cN,
    const label faceI,
    const scalar blendingFactor
) const
{
    // Calculate upwind value, faceFlux C tilde and do a stabilisation

    scalar phict = 0;
    scalar phiupw = 0;

    if (faceFlux > 0)
    {
        phiupw = phiN - 2*(gradcP & d);

        phiupw = max(min(phiupw, vsfMax_), vsfMin_);

        // limit virtual upwind node
        if (blendingFactor != 0)
        {
            const point& p =  cP - (cN - cP);
            phiupw = limitVirtualUpwind(phi, p, faceI, faceFlux, phiupw);
        }

        if ((phiN - phiupw) > 0)
        {
            phict = (phiP - phiupw)/(phiN - phiupw + SMALL);
        }
        else
        {
            phict = (phiP - phiupw)/(phiN - phiupw - SMALL);
        }
    }
    else
    {
        phiupw = phiP + 2*(gradcN & d);

        phiupw = max(min(phiupw, vsfMax_), vsfMin_);

        // limit virtual upwind node
        if (blendingFactor != 0)
        {
            const point& p =  cN - (cP - cN);
            phiupw = limitVirtualUpwind(phi, p, faceI, faceFlux, phiupw);
        }

        if ((phiP - phiupw) > 0)
        {
            phict = (phiN - phiupw)/(phiP - phiupw + SMALL);
        }
        else
        {
            phict = (phiN - phiupw)/(phiP - phiupw - SMALL);
        }
    }

    // Calculate the weighting factors for MCICSAMblended

    scalar phifupwt = 0;
    scalar phifIGAMMAblended = 0; // blending between gamma and interGamma
    scalar phifGAMMA = 0; // gamma scheme value
    scalar phifIGAMMA = 0; // interGamma scheme value
    scalar weight;

    // Calculate UDS-Scheme value
    phifupwt = phict;

    // Calculate the weighting factors for interGAMMAblended
    if (phict > 0 && phict <= 0.5 )
    {
        scalar gammaC = 2*phict;
        phifIGAMMA = (scalar(1)-gammaC)*phict + gammaC;
        phifGAMMA = gammaC - pow(phict,2.);
        phifIGAMMAblended = blendingFactor * phifIGAMMA + (scalar(1)-blendingFactor) * phifGAMMA;
        weight = (phifIGAMMAblended - phict)/(1 - phict);
    }
    else if (phict > 0.5 && phict < 1) // blending between downwind and CD
    {
        phifIGAMMA = scalar(1);
        phifGAMMA = scalar(0.5) + scalar(0.5) * phict;
        phifIGAMMAblended = blendingFactor + (scalar(1)-blendingFactor) * phifGAMMA;
        weight = (phifIGAMMAblended - phict)/(1 - phict);
    }
    else
    {
        weight = 0;                    // use upwind
    }

    if (faceFlux > 0)
    {
        return 1 - weight;
    }
    else
    {
        return weight;
    }
}


void Foam::interGAMMAblended::calculateBlendingFactor() const
{
    const fvMesh& mesh = this->mesh();
    const surfaceScalarField& isInterfacef = mesh.objectRegistry::lookupObject<const surfaceScalarField> ("isInterfacef");
    
    // Blending factor for interGAMMAblended:
    blendingFactor_ = pos(isInterfacef - 1e-06);
    //blendingFactor_ = blendingFactor_*scalar(0.) + scalar(1.); // test interGamma
    //blendingFactor_ *= 0.;   // test Gamma
}


namespace Foam
{
//defineNamedTemplateTypeNameAndDebug(interGAMMAblended, 0);
defineTypeNameAndDebug(interGAMMAblended, 0);

surfaceInterpolationScheme<scalar>::addMeshConstructorToTable<interGAMMAblended>
    addinterGAMMAblendedMeshConstructorToTable_;

surfaceInterpolationScheme<scalar>::addMeshFluxConstructorToTable<interGAMMAblended>
    addinterGAMMAblendedMeshFluxConstructorToTable_;

limitedSurfaceInterpolationScheme<scalar>::addMeshConstructorToTable<interGAMMAblended>
    addinterGAMMAblendedMeshConstructorToLimitedTable_;

limitedSurfaceInterpolationScheme<scalar>::
addMeshFluxConstructorToTable<interGAMMAblended>
    addinterGAMMAblendedMeshFluxConstructorToLimitedTable_;
}

// ************************************************************************* //
