/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

\*---------------------------------------------------------------------------*/

#include "interfaceCapturingScheme.H"
#include "fvc.H"
#include "volFields.H"
#include "surfaceFields.H"
#include "upwind.H"
#include "point.H"
#include <set>

// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //

Foam::scalar Foam::interfaceCapturingScheme::limiter
(
    const volScalarField& phi,
    const scalar cdWeight,
    const scalar faceFlux,
    const scalar& phiP,
    const scalar& phiN,
    const vector& gradcP,
    const vector& gradcN,
    const scalar Cof,
    const vector d,
    const point cP,
    const point cN,
    const label faceI,
    const scalar blendingFactor
) const
{
    // Calculate interfaceCapturingScheme weight
    scalar w =
        weight
        (
            phi,
            cdWeight,
            faceFlux,
            phiP,
            phiN,
            gradcP,
            gradcN,
            Cof,
            d,
            cP,
            cN,
            faceI,
            blendingFactor
        );

    // Calculate interfaceCapturingScheme face value
    scalar phif = w*phiP + (1 - w)*phiN;

    // Calculate UD and CD face value
    scalar phiU = faceFlux >= 0 ? phiP : phiN;
    scalar phiCD = cdWeight*phiP + (1 - cdWeight)*phiN;

    // Calculate the effective limiter for the QUICK interpolation
    scalar CLimiter = (phif - phiU)/stabilise(phiCD - phiU, SMALL);

    Info << "Foam::scalar Foam::interfaceCapturingScheme::limiter is called ...\n" << endl;
    // Limit the limiter between upwind and downwind
    return max(min(CLimiter, 2), 0);
}


Foam::tmp<Foam::surfaceScalarField> Foam::interfaceCapturingScheme::limiter
(
    const volScalarField& phi
) const
{
    const fvMesh& mesh = this->mesh();

    tmp<surfaceScalarField> tLimiter
    (
        new surfaceScalarField
        (
            IOobject
            (
                type() + "Limiter(" + phi.name() + ')',
                mesh.time().timeName(),
                mesh
            ),
            mesh,
            dimless
        )
    );
    surfaceScalarField& lim = tLimiter.ref();

    volVectorField gradc = fvc::grad(phi);

    const surfaceScalarField faceFlux = this->faceFlux_;
/*
    surfaceScalarField magPhi = mag(faceFlux);
    surfaceScalarField SfUfbyDelta =
        mesh.surfaceInterpolation::deltaCoeffs()*magPhi;
    surfaceScalarField Cof = (SfUfbyDelta/mesh.magSf())*mesh.time().deltaT();

    surfaceScalarField Cof =
        mesh.time().deltaT()
       *upwind<scalar>(mesh, faceFlux_).interpolate
        (
            fvc::surfaceIntegrate(faceFlux_)
        );
*/
    volScalarField outFlux = fvc::surfaceSum(mag(faceFlux));
    outFlux.primitiveFieldRef() /= mesh.V();

    // this calculation is only valid for a divergence-free flux field!!!
    // otherwise: need to sum up all fluxes leaving the centre cell
    surfaceScalarField Cof =
        0.5 * mesh.time().deltaT()
       *upwind<scalar>(mesh, faceFlux).interpolate
        (
            outFlux
        );

    calculateTheta();
    calculateBlendingFactor();

    const surfaceScalarField& CDweights = mesh.surfaceInterpolation::weights();

    const unallocLabelList& owner = mesh.owner();
    const unallocLabelList& neighbour = mesh.neighbour();

    const vectorField& C = mesh.C();

    scalarField& pLim = lim.primitiveFieldRef();

    forAll(pLim, faceI)
    {
        label own = owner[faceI];
        label nei = neighbour[faceI];

        vector d = C[nei] - C[own];

        pLim[faceI] = limiter
        (
            phi,
            CDweights[faceI],
            this->faceFlux_[faceI],
            phi[own],
            phi[nei],
            gradc[own],
            gradc[nei],
            Cof[faceI],
            d,
            C[own],
            C[nei],
            faceI,
            blendingFactor_[faceI]
        );
    }

    surfaceScalarField::Boundary& bLim = lim.boundaryFieldRef();
    surfaceScalarField::Boundary& bCof = Cof.boundaryFieldRef();
    forAll(bLim, patchi)
    {
        scalarField& pLim = bLim[patchi];

        if (bLim[patchi].coupled())
        {
            const scalarField& pCDweights = CDweights.boundaryField()[patchi];

            const scalarField& pFaceFlux =
                this->faceFlux_.boundaryField()[patchi];

            scalarField pphiP =
                phi.boundaryField()[patchi].patchInternalField();

            scalarField pphiN =
                phi.boundaryField()[patchi].patchNeighbourField();

            vectorField pGradcP =
                gradc.boundaryField()[patchi].patchInternalField();

            vectorField pGradcN =
                gradc.boundaryField()[patchi].patchNeighbourField();

            const scalarField& pCof = Cof.boundaryField()[patchi];
            const scalarField& pblendingFactor = blendingFactor_.boundaryField()[patchi];

            // Build the d-vectors
            // Better version of d-vectors: Zeljko Tukovic, 25/Apr/2010
            vectorField pd = bLim[patchi].patch().delta();

            forAll(pLim, faceI)
            {
                pLim[faceI] = limiter
                (
                    phi,
                    pCDweights[faceI],
                    pFaceFlux[faceI],
                    pphiP[faceI],
                    pphiN[faceI],
                    pGradcP[faceI],
                    pGradcN[faceI],
                    pCof[faceI],
                    pd[faceI],
                    pTraits<vector>::zero,
                    pTraits<vector>::zero,
                    faceI,
                    pblendingFactor[faceI]
                );
            }
        }
        else
        {
            pLim = 1.0;
        }
    }

    return tLimiter;
}


Foam::scalar Foam::interfaceCapturingScheme::weight
(
    const volScalarField& phi,
    const scalar cdWeight,
    const scalar faceFlux,
    const scalar& phiP,
    const scalar& phiN,
    const vector& gradcP,
    const vector& gradcN,
    const scalar Cof,
    const vector d,
    const point cP,
    const point cN,
    const label faceI,
    const scalar blendingFactor
) const
{
    scalar weight = 0;

    if (faceFlux > 0)
    {
        return 1 - weight;
    }
    else
    {
        return weight;
    }
}


Foam::tmp<Foam::surfaceScalarField> Foam::interfaceCapturingScheme::calcWeights
(
    const volScalarField& phi
) const
{
    const fvMesh& mesh = this->mesh();

    tmp<surfaceScalarField> tWeightingFactors
    (
        new surfaceScalarField(mesh.surfaceInterpolation::weights())
    );
    surfaceScalarField& weightingFactors = tWeightingFactors.ref();

    scalarField& weights = weightingFactors.primitiveFieldRef();

    volVectorField gradc = fvc::grad(phi);

    const surfaceScalarField faceFlux = this->faceFlux_;
/*
    surfaceScalarField magPhi = mag(faceFlux);
    surfaceScalarField SfUfbyDelta =
        mesh.surfaceInterpolation::deltaCoeffs()*magPhi;
    surfaceScalarField Cof = (SfUfbyDelta/mesh.magSf())*mesh.time().deltaT();

    surfaceScalarField Cof =
        mesh.time().deltaT()
       *upwind<scalar>(mesh, faceFlux_).interpolate
        (
            fvc::surfaceIntegrate(faceFlux_)
        );
*/

    volScalarField outFlux = fvc::surfaceSum(mag(faceFlux));
    outFlux.primitiveFieldRef() /= mesh.V();

    //- fix mass conservation problems with coupled boundaries (parallel runs)
    //  i.e. correct calculation of local Courant number at coupled boundaries
    if (Pstream::parRun())
    {
        outFlux.correctBoundaryConditions();
    }

    surfaceScalarField Cof =
        0.5 * mesh.time().deltaT()
       *upwind<scalar>(mesh, faceFlux).interpolate
        (
            outFlux
        );

    calculateTheta();
    calculateBlendingFactor();

    const surfaceScalarField& CDweights = mesh.surfaceInterpolation::weights();

    const unallocLabelList& owner = mesh.owner();
    const unallocLabelList& neighbour = mesh.neighbour();

    const vectorField& C = mesh.C();

    scalarField& w = weightingFactors.primitiveFieldRef();

    forAll(w, faceI)
    {
        label own = owner[faceI];
        label nei = neighbour[faceI];

        vector d = C[nei] - C[own];

        w[faceI] = weight
        (
            phi,
            CDweights[faceI],
            this->faceFlux_[faceI],
            phi[own],
            phi[nei],
            gradc[own],
            gradc[nei],
            Cof[faceI],
            d,
            C[own],
            C[nei],
            faceI,
            blendingFactor_[faceI]
        );
    }

    surfaceScalarField::Boundary& bWeights =
        weightingFactors.boundaryFieldRef();

    surfaceScalarField::Boundary& bCof = Cof.boundaryFieldRef();

    forAll(bWeights, patchi)
    {
        scalarField& pWeights = bWeights[patchi];

        if (bWeights[patchi].coupled())
        {
            const scalarField& pCDweights = CDweights.boundaryField()[patchi];

            const scalarField& pFaceFlux =
                this->faceFlux_.boundaryField()[patchi];

            scalarField pphiP =
                phi.boundaryField()[patchi].patchInternalField();

            scalarField pphiN =
                phi.boundaryField()[patchi].patchNeighbourField();

            vectorField pGradcP =
                gradc.boundaryField()[patchi].patchInternalField();

            vectorField pGradcN =
                gradc.boundaryField()[patchi].patchNeighbourField();

            const scalarField& pCof = Cof.boundaryField()[patchi];
            const scalarField& pblendingFactor = blendingFactor_.boundaryField()[patchi];

            // Build the d-vectors
            // Better version of d-vectors: Zeljko Tukovic, 25/Apr/2010
            vectorField pd = bWeights[patchi].patch().delta();

            forAll(pWeights, faceI)
            {
                pWeights[faceI] = weight
                (
                    phi,
                    pCDweights[faceI],
                    pFaceFlux[faceI],
                    pphiP[faceI],
                    pphiN[faceI],
                    pGradcP[faceI],
                    pGradcN[faceI],
                    pCof[faceI],
                    pd[faceI],
                    pTraits<vector>::zero,
                    pTraits<vector>::zero,
                    faceI,
                    pblendingFactor[faceI]
                );
            }
        }
        else
        {
            pWeights = 1.0;
        }
    }

    if (surfaceInterpolation::debug)
    {
    
        surfaceScalarField blendingFactor
        (
            IOobject
            (
                "blendingFactor",
                mesh.time().timeName(),
                mesh,
                IOobject::NO_READ,
                IOobject::AUTO_WRITE
            ),
            blendingFactor_
        );
        
        if (mesh.time().write())
        {
            blendingFactor.write();
        }
    }

    return tWeightingFactors;
}


Foam::tmp<Foam::surfaceScalarField> Foam::interfaceCapturingScheme::correction
(
    const volScalarField& vf
) const
{
    const fvMesh& mesh = this->mesh();
    const surfaceScalarField faceFlux = this->faceFlux_;

    // Correction = full interpolation minus upwinded part
    tmp<surfaceScalarField> tcorr
    (
        new surfaceScalarField
        (
            IOobject
            (
                type() + "correction(" + vf.name() + ')',
                mesh.time().timeName(),
                mesh
            ),
            mesh,
            vf.dimensions()
        )
    );
    
    surfaceScalarField& corr = tcorr.ref();
    
    upwind<scalar> upwindScheme(mesh, faceFlux);

    corr = surfaceInterpolationScheme<scalar>::interpolate(vf, calcWeights(vf)) - upwindScheme.interpolate(vf);
    //corr *= scalar(0); // for testing!
    
    return tcorr;
}


void Foam::interfaceCapturingScheme::calculateTheta() const
{
    // Blending only in narrow band around interface
    const fvMesh& mesh = this->mesh();

    const surfaceScalarField& isInterfacef = mesh.objectRegistry::lookupObject<const surfaceScalarField> ("isInterfacef");
    if (blendingField_.find("nHatfReconalpha1") != std::string::npos)
    {
        blendingFactor_ = mesh.objectRegistry::lookupObject<const surfaceScalarField> (blendingField_)/mesh.magSf();
    }
    else
    {
        const volScalarField& vsf = mesh.objectRegistry::lookupObject<const volScalarField> (blendingField_);
        const volVectorField vsfGrad(fvc::grad(vsf,"alphaS"));
        blendingFactor_ = 
        (
            linearInterpolate(
                vsfGrad/(mag(vsfGrad)+deltaN_)
            )
            &
            mesh.Sf()/mesh.magSf()
        );
    }
    blendingFactor_ *= isInterfacef;
}


void Foam::interfaceCapturingScheme::calculateBlendingFactor() const
{
    // Blending factor for CICSAM:
    scalar kf = 1.0; //0.5; // 1.3;
    blendingFactor_ = min(blendingFactor_, scalar(1.));
    blendingFactor_ = cos(2*(acos(blendingFactor_)));
    blendingFactor_ = kf * (blendingFactor_ + 1)/2;
    blendingFactor_ = max(min(blendingFactor_, scalar(1.)), scalar(0.));
}


Foam::scalar Foam::interfaceCapturingScheme::limitVirtualUpwind
(
    const volScalarField& phi,
    const point& p,
    const label faceI,
    const scalar faceFlux,
    scalar phiVU
) const
{
    //const fvMesh& mesh = vsf_.mesh();
    const fvMesh& mesh = this->mesh();

    // get cell ID of centre cell (U->C->D)
    label own = mesh.faceOwner()[faceI];
    label nei = mesh.faceNeighbour()[faceI];

    label seedCell = nei;

    if (faceFlux > 0)
    {
        seedCell = own;
    }

    // get nearest cell to virtual upwind point p (Algorithm: Tomislav Maric)
    label nearestCell = cellContainingPoint(p, mesh, seedCell);

    // get all neighbour cells of cellID and limit virtual upwind with min/max
    labelList cellNeighbours = mesh.cellCells()[nearestCell];

    List<scalar> phiCells;

    forAll(cellNeighbours, I)
    {
        phiCells.append(phi[cellNeighbours[I]]); // should be phi (field to be interpolated) instead vsf_ (alphaField)
    }

    scalar minPhiCells = min(phiCells);
    scalar maxPhiCells = max(phiCells);

    // copy phiVU to compare with new one
    scalar phiVUcp = phiVU;

    // limit virtual upwind value
    phiVU = max(minPhiCells, min(phiVU, maxPhiCells));

    if (surfaceInterpolation::debug)
    {
        if (phiVUcp != phiVU)
        {
            Info<< " limit virtual upwind node in cell: " << nearestCell << endl;
            isLimited_[nearestCell] = 1;

            /*
            // check algorithm for uniform carthesian grid:
            point c = mesh.C()[nearestCell];
            if (mag(c-p) > 1e-07)
            {
                Info<< "search algorithm found wrong cell ..." << endl;
            }
            */
        }
    }
    
    return phiVU;
}


bool Foam::interfaceCapturingScheme::pointIsInCell
(
    const point p, 
    const label cellLabel, 
    const fvMesh& mesh, 
    scalar tolerance
) const
{
    const cellList& cells = mesh.cells(); 
    const cell& cell = cells[cellLabel];
    const labelList& own = mesh.faceOwner(); 
    const vectorField& Cf = mesh.faceCentres(); 
    const vectorField& Sf = mesh.faceAreas(); 

    bool pointIsInside = true;

    // For all face labels of the cell.
    forAll (cell, I) 
    {
        label faceLabel = cell[I];
        vector faceNormal = Sf[faceLabel];

        // If the cell does not own the face.
        if (! (cellLabel == own[cell[I]])) 
        {
            faceNormal *= -1;  
        }

        // Compute the vector from the face center to the point p.
        vector fp = p - Cf[cell[I]];  

        if ((fp & faceNormal) > tolerance) 
        {
            //Info << "point outside face = " << (fp & faceNormal) << endl; 
            pointIsInside = false;
            break;
        }
        //else
        //{
        //Info << "point inside face = " << (fp & faceNormal) << endl; 
        //}
    }

    return pointIsInside;
}



Foam::labelList Foam::interfaceCapturingScheme::pointCellStencil
(
    label cellLabel, 
    const fvMesh& mesh
) const
{
    const faceList& faces = mesh.faces(); 
    const cellList& cells = mesh.cells(); 
    const labelListList& pointCells = mesh.pointCells(); 

    labelList cellPoints = cells[cellLabel].labels(faces); 

    List<label> newNeighborCells;

    forAll (cellPoints, I)
    {
        newNeighborCells.append(pointCells[cellPoints[I]]);
    }
    
    return newNeighborCells;
}



Foam::label Foam::interfaceCapturingScheme::cellContainingPoint
(
    const point& p, 
    const fvMesh& mesh, 
    const label seedCell 
) const 
{
    if (pointIsInCell(p, seedCell, mesh))
    {
        return seedCell;  
    }

    const volVectorField& C = mesh.C(); 
    scalar minDistance = mag(C[seedCell] - p);
    label minDistanceCell = seedCell;

    // For all neighbour cells of the seed cell. 
    const labelListList& cellCells = mesh.cellCells(); 
    labelList neighborCells = cellCells[seedCell]; 

    const cellList& cells = mesh.cells(); 

    // Extend the tetrahedral stencil.
    if (cells[seedCell].size() == 4)
    {
        neighborCells = pointCellStencil(seedCell, mesh);  
    }

    //Info << "used stencil = " << neighborCells  << endl;
    forAll (neighborCells, I) 
    {
        label neighborCell = neighborCells[I]; 

        if (pointIsInCell(p, neighborCell, mesh)) 
        {
            lastDistance_ = minDistance; 
            return neighborCell; 
        }

        scalar distance = mag(C[neighborCell] - p); 

        // Set label of the cell with the minimal distance. 
        if (distance <= minDistance) 
        {
            minDistance = distance; 
            minDistanceCell = neighborCell; 
        }
    }

    if (pointIsInCell(p, minDistanceCell, mesh)) 
    {
        lastDistance_ = minDistance; 
        return minDistanceCell; 
    }
    else 
    {
        if (mag(lastDistance_ - minDistance) < SMALL)
        {
            return minDistanceCell; 
        }
        else
        {
            lastDistance_ = minDistance; 
            // Recursion
            return cellContainingPoint(p, mesh, minDistanceCell); 
        }
    }

    return -1; 
}


namespace Foam
{
//defineNamedTemplateTypeNameAndDebug(interfaceCapturingScheme, 0);
defineTypeNameAndDebug(interfaceCapturingScheme, 0);

surfaceInterpolationScheme<scalar>::addMeshConstructorToTable<interfaceCapturingScheme>
    addinterfaceCapturingScheMeshConstructorToTable_;

surfaceInterpolationScheme<scalar>::addMeshFluxConstructorToTable<interfaceCapturingScheme>
    addinterfaceCapturingSchemeMeshFluxConstructorToTable_;

limitedSurfaceInterpolationScheme<scalar>::addMeshConstructorToTable<interfaceCapturingScheme>
    addinterfaceCapturingSchemeMeshConstructorToLimitedTable_;

limitedSurfaceInterpolationScheme<scalar>::
addMeshFluxConstructorToTable<interfaceCapturingScheme>
    addinterfaceCapturingSchemeMeshFluxConstructorToLimitedTable_;
}

// ************************************************************************* //
